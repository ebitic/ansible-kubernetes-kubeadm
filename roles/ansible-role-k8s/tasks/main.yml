---
- meta: flush_handlers

- include_tasks: debian.yaml
  when: ansible_os_family == "Debian"

- name: Kubeadm init for the first master
  command: kubeadm init --control-plane-endpoint={{ ansible_host }} --pod-network-cidr={{ pod_network_cidr }} --ignore-preflight-errors Swap --ignore-preflight-errors NumCPU
  when: inventory_hostname in groups["k8s_first_master"]

- name: Create .kube directory for non root user
  file: 
    path: /home/{{ ansible_user }}/.kube
    state: directory
    owner: "{{ ansible_user }}"
    group: "{{ ansible_user }}"
  when: ansible_user != "root" and inventory_hostname in groups["k8s_first_master"]

- name: Copy kubernetes config
  copy:
    src: /etc/kubernetes/admin.conf
    dest: /home/{{ ansible_user }}/.kube/config
    remote_src: yes
  when: ansible_user != "root" and inventory_hostname in groups["k8s_first_master"]

- name: Create .kube directory for root user
  file: 
    path: /root/.kube
    state: directory
    owner: root
    group: root
  when: ansible_user == "root" and inventory_hostname in groups["k8s_first_master"]

- name: Copy kubernetes config
  copy:
    src: /etc/kubernetes/admin.conf
    dest: /root/.kube/config
    remote_src: yes
  when: ansible_user == "root" and inventory_hostname in groups["k8s_first_master"]

- name: Create kubeadm join command
  command: kubeadm token create --print-join-command
  register: kubeadmjoin
  when: inventory_hostname in groups["k8s_first_master"]

- name: Save Kubeadm join command
  become: false
  local_action:
    module: copy
    content: "{{kubeadmjoin['stdout']}}"
    dest: kubeadmjoin
  when: inventory_hostname in groups["k8s_first_master"]

- name: Import Kubeadm join command file
  copy:
    src: kubeadmjoin
    dest: /tmp/kubeadmjoin
  when: inventory_hostname in groups["k8s_masters"] or inventory_hostname in groups["k8s_workers"]

- name: Join kubeadm cluster as a controlplane
  command: bash /tmp/kubeadmjoin --control-plane
  when: inventory_hostname in groups["k8s_masters"]

- name: Join kubeadm cluster as a worker
  command: bash /tmp/kubeadmjoin
  when: inventory_hostname in groups["k8s_workers"]

- name: Save Kubeadm join command
  become: false
  local_action:
    module: file
    path: kubeadmjoin
    state: absent

- name: Download weave yaml file
  get_url:
    url: https://github.com/weaveworks/weave/releases/download/v{{ weave_version }}/weave-daemonset-k8s.yaml
    dest: /tmp/weave-daemonset-k8s.yaml
  when: inventory_hostname in groups["k8s_first_master"] and setup_weave

- name: Deploy weave components
  command: kubectl apply -f /tmp/weave-daemonset-k8s.yaml
  when: inventory_hostname in groups["k8s_first_master"] and setup_weave